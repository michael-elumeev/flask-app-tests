import os
import requests


def label_finder(p, label_value):
    matching = False
    for label in p["labels"]:
        if label["name"] == label_value:
            matching = True
    return matching


def get_list_for_all(state):
    TOKEN = os.getenv("TOKEN")
    HEADERS = {'Authorization': f'Bearer {TOKEN}'}
    URL_API = "https://api.github.com/repos/boto/boto3/pulls"
    PARAMS = {
        "state": "all",
        "per_page": 100
    }
    answer = requests.get(url=URL_API, headers=HEADERS, params=PARAMS)
    if answer.status_code == 200:
        if state == "open":
            return [p for p in answer.json() if p["state"] == "open"]
        elif state == "closed":
            return [p for p in answer.json() if p["state"] == "closed"]
        elif state == "bug":
            return [p for p in answer.json() if p["state"] == "open" and label_finder(p, "bug")]
        elif state == "needs-review":
            return [p for p in answer.json() if p["state"] == "open" and label_finder(p, "needs-review")]


def get_pull_requests(state):
    print("State:", state )
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """
    all_info = get_list_for_all(state)
    if all_info:
        answer = [{"title": p["title"], "num": p["number"], "link": p["html_url"]} for p in all_info]
        return answer
    else:
        return []
