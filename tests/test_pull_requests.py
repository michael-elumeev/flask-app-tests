import pytest
from mock import Mock, MagicMock, patch, call, ANY, mock_open
from faker import Faker
from faker.providers import python
import json
import responses

import handlers.pull_requests as sut

faker = Faker()
faker.add_provider(python)

def all_prs():
    answer =  [{"state": "open", "title": faker.pystr(), "number": faker.pystr(),
                "html_url": faker.pystr()} for _ in range(10)]
    answer.extend([{"state": "closed", "title": faker.pystr(), "number": faker.pystr(),
                    "html_url": faker.pystr()} for _ in range(10)])
    return answer

def generate_pr():
    return {
        "info": faker.pystr(),
        "labels": [
            {"name":"1"},
            {"name":"2"},
            {"name":"3"},
            {"name":"bug"},
            {"name":"needs-review"}
        ]
    }

def test_label_finder():
    mock_pr = generate_pr()

    result = sut.label_finder(mock_pr, "1")
    assert result == True

    result = sut.label_finder(mock_pr, "2")
    assert result == True

    result = sut.label_finder(mock_pr, "bug")
    assert result == True

    result = sut.label_finder(mock_pr, "5")
    assert result == False

@responses.activate
@patch('handlers.pull_requests.label_finder')
@patch('os.getenv')
def test_get_list_for_all_open(mock_get_env, mock_label_finder):
    URL = "https://api.github.com/repos/boto/boto3/pulls"
    responses.add(responses.GET, URL, json=all_prs(), status=200)
    answer = sut.get_list_for_all(state="open")
    assert len(answer) == 10

    answer = sut.get_list_for_all(state="closed")
    assert len(answer) == 10

    mock_label_finder.return_value = True
    answer = sut.get_list_for_all(state="bug")
    assert len(answer) == 10

    mock_label_finder.return_value = True
    answer = sut.get_list_for_all(state="needs-review")
    assert len(answer) == 10


@patch('handlers.pull_requests.get_list_for_all')
def test_get_pull_requests(mock_get_list):
    prs_to_return = all_prs()
    mock_get_list.return_value = prs_to_return
    mock_answer = [{"title": p["title"], "num": p["number"], "link": p["html_url"]} for p in prs_to_return]

    answer_to_check = sut.get_pull_requests(state="open")
    assert answer_to_check == mock_answer


# from mock import patch
# import json
# from handlers.pull_requests import get_pull_requests
# from unittest import TestCase
#
# class TestStateRequests(TestCase):
#     @patch('requests.get')
#     def test_state_requests(self, get_mock):
#         # use side_effect instead of return_value to provide several values
#         states = ["open", "closed", "bug", "needs-review", "some_stuff"]
#         get_mock.return_value.status_code = 200
#         with open ("tests/sample_answer.json") as file:
#             get_mock.return_value.json.return_value = json.load(file)
#         for state in states:
#             print(f"Checking the state: {state}")
#             with open(f"tests/json_answer_{state}.json") as f:
#                 expected_res = json.load(f)
#             res = get_pull_requests(state)
#             self.assertEqual(res, expected_res)


# from mock import patch
# import json
# from handlers.pull_requests import get_pull_requests
# from unittest import TestCase

# class TestStateRequests(TestCase):
#     @patch('requests.get')
#     def test_state_requests(self, get_mock):
#         # use side_effect instead of return_value to provide several values
#         states = ["open", "closed", "bug", "needs-review", "some_stuff"]
#         get_mock.return_value.status_code = 200
#         with open ("tests/sample_answer.json") as file:
#             get_mock.return_value.json.return_value = json.load(file)
#         for state in states:
#             print(f"Checking the state: {state}")
#             with open(f"tests/json_answer_{state}.json") as f:
#                 expected_res = json.load(f)
#             res = get_pull_requests(state)
#             self.assertEqual(res, expected_res)

